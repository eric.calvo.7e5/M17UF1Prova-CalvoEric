using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FragateSpawner : MonoBehaviour
{
    public GameObject Fragate;
    public float tiempoSpawn = 1f;

    // Start is called before the first frame update
    private void Start()
    {
        FragateSpawn();
    }

    // Update is called once per frame
    private void Update()
    {
    }

    private void FragateSpawn()
    {
        Instantiate(Fragate, new Vector3(Random.Range(-9, 9), 12f, 0f), Quaternion.identity);
        Invoke("FragateSpawn", tiempoSpawn);
    }
}