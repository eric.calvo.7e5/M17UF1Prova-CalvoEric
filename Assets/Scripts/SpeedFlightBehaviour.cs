using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedFlightBehaviour : MonoBehaviour
{
    public float score;

    // Start is called before the first frame update
    private void Start()
    {
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("fireball"))
        {
            ScoreSum();

            Destroy(gameObject);
        }
        if (collision.CompareTag("Player") || collision.CompareTag("Limite"))
        {
            Destroy(gameObject);
        }
    }

    // Update is called once per frame
    private void Update()
    {
        PlayerPrefs.SetFloat("score", score);
    }

    private void ScoreSum()
    {
        score += 5;
    }
}