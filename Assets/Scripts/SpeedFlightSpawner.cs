using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedFlightSpawner : MonoBehaviour
{
    public GameObject Speed;
    public float tiempoSpawn = 0.5f;

    // Start is called before the first frame update
    private void Start()
    {
        SpeedSpawn();
    }

    // Update is called once per frame
    private void Update()
    {
    }

    private void SpeedSpawn()
    {
        Instantiate(Speed, new Vector3(Random.Range(-9, 9), 11f, 0f), Quaternion.identity);
        Invoke("SpeedSpawn", tiempoSpawn);
    }
}