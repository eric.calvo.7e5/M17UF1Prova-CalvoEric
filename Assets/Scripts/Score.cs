using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Score : MonoBehaviour
{
    // Start is called before the first frame update
    private void Start()
    {
    }

    // Update is called once per frame
    private void Update()
    {
        gameObject.GetComponent<TMP_Text>().text = "Score: " + (PlayerPrefs.GetFloat("score") + PlayerPrefs.GetFloat("+score"));
    }
}