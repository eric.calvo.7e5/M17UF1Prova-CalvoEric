using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShipController : MonoBehaviour
{
    public GameObject fireball;
    private float fireballForce;

    // Start is called before the first frame update
    private void Start()
    {
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetKey("right") || Input.GetKey("d") || Input.GetKey("left") || Input.GetKey("a") || Input.GetKey("up") || Input.GetKey("w") || Input.GetKey("down") || Input.GetKey("s"))
        {
            Walk();
        }
        if (Input.GetKeyDown("space"))
        {
            Shoot();
        }
    }

    private void Walk()
    {
        if (Input.GetKey("right") || Input.GetKey("d"))
        {
            gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(1500F * Time.deltaTime, 0));
        }

        if (Input.GetKey("left") || Input.GetKey("a"))
        {
            gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(-1500F * Time.deltaTime, 0));
        }

        if ((Input.GetKey("up") || Input.GetKey("w")))
        {
            gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 1500F * Time.deltaTime));
        }
        if ((Input.GetKey("down") || Input.GetKey("s")))
        {
            gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, -1500F * Time.deltaTime));
        }
    }

    private void Shoot()
    {
        var shoot = Instantiate(fireball, transform.position, Quaternion.identity);
        shoot.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 10000F * Time.deltaTime));
    }
}