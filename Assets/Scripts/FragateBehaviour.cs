using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FragateBehaviour : MonoBehaviour
{
    private float contador;
    public GameObject bullet;
    public float score = 0;

    // Start is called before the first frame update
    private void Start()
    {
        Invoke("Shoot", Random.Range(3f, 9f));
    }

    // Update is called once per frame
    private void Update()
    {
        PlayerPrefs.SetFloat("+score", score);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("fireball"))
        {
            contador++;
            if (contador == 5f)
            {
                ScoreSum();

                Destroy(gameObject);
                contador = 0f;
            }
        }
        if (collision.CompareTag("Limite"))
        {
            Destroy(gameObject);
        }
    }

    private void Shoot()
    {
        var shoot = Instantiate(bullet, transform.position, Quaternion.identity);
        shoot.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 10000F * Time.deltaTime));
    }

    private void ScoreSum()
    {
        score += 20;
    }
}